
const data = {
  header: {
    name: "sai",
    address: "746 N i",
    city: "mysore",
    state: "AZ",
    zip: "84920",
    phone: "6105453672",
    email: "xyzk@email.com",
    summary:
      "abcdefghijklmnopqrst",
  },
  professional: {
    company1: "abc",
    local1: "Salt Lake ",
    position1: "Manager",
    start1: "Jan/2019",
    end1: "Apr/2020",
    desc1: [
      "Responsible for all the material handled .",
      "Help others on their daily tasks.",
      "Professional photographer hired by others companies.",
    ],
    company2: "GProductions",
    local2: "ny",
    position2: "Intern",
    start2: "Jul/2018",
    end2: "Dec/2018",
    desc2: [
      "Print and fax documents for the entire company.",
      "Assist CEO on daily tasks.",
      "Provide assistance to all employees.",
    ],
  },
  education: {
    institution: "ab University",
    city: "USA",
    major: "Bachelors in computer",
    gradYear: "2017",
    additional: "GPA 8.56",
  },
  additional: [
    "5+ years of experience with Microsoft Office",
    "English and Spanish speaker",
    "Adaptability",
    "Interpersonal Communication",
    "Friend of all",
  ],
};

export default data;
